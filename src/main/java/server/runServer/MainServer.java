package server.runServer;

import server.socket.SocketServer;

import java.io.IOException;

/**
 * Created by Евгений on 16.09.2017.
 */
public class MainServer {
    public static void main(String[] args) throws IOException {
        SocketServer socketServer = new SocketServer();
        socketServer.start();
    }
}
