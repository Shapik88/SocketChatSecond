package client.runClient;

import client.socket.SocketClient;

import java.io.IOException;

/**
 * Created by Евгений on 16.09.2017.
 */
public class MainClient {

    public static void main(String[] args) throws IOException, InterruptedException {
        SocketClient socketClient = new SocketClient();
        socketClient.connect();
    }
}
